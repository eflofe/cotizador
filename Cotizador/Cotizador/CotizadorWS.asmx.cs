﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Cotizador.LaLatino;
using Newtonsoft.Json;

namespace Cotizador
{
    /// <summary>
    /// Descripción breve de WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    //[System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {
        private string pass = "";
        private string usr = "";
        private Seguro seguro;
        private string logWSCot;
        private CotizadorQualitas qualitas;
        private BanorteCot banorte;
        private LatinoCot latino;
        private LogCotizacion log = new LogCotizacion();





        [WebMethod]
        public string cotizador(string usuario, string pass,string data, string movimiento)
        {           
            if (this.pass==pass&&this.usr==usuario)
            {
                try
                   {
                    string salida = "";

                    try
                    {
                        this.seguro = new Seguro();
                        this.seguro = JsonConvert.DeserializeObject<Seguro>(data);
                    }
                    catch(Exception exc)
                    {
                        throw new ArgumentException("Ah habido un error con los datos del JSON " + exc.Message);
                    }

                    if (movimiento == "cotizacion")
                        logWSCot = log.InsertLog(data,seguro.Aseguradora,seguro.Vehiculo.Marca,seguro.Vehiculo.Modelo.ToString(),seguro.Vehiculo.Descripcion,seguro.Vehiculo.Clave); // aqui va la funcion de insercion en el log
                    else if (movimiento == "emision")
                        logWSCot = "emision"; // aqui va la funcion de insercion en el log
                    else
                    {
                        throw new ArgumentException("Ah habido un error con los datos del movimiento");
                    }
                    switch (seguro.Aseguradora)
                    {
                        case "BANORTE":
                            {
                                if (movimiento == "cotizacion")
                                {
                                    banorte = new BanorteCot();
                                    salida = banorte.BanorteCotizacionResponse(seguro, logWSCot);
                                    
                                }
                                else
                                {
                                    //Funcion de emision  salida = banorte.BanorteCotizacionResponse(seguro, logWSCot);
                                }
                                break;
                            }
                        case "QUALITAS":
                            {
                                if (movimiento == "cotizacion")
                                {
                                   qualitas = new CotizadorQualitas(); // Instancia para el objeto de la clase qualitas
                                   //salida=  //Prueba para inserción de log
                                  
                                 // salida = qualitas.CotizaQualitas(seguro,logWSCot); 
                                }
                                else
                                {
                                    //Funcion de emision  
                                }
                                break;
                            }
                        case "LATINO":
                            {
                                if (movimiento == "cotizacion")
                                {
                                    latino = new LatinoCot();
                                    salida = latino.CotizacionLatino(seguro, logWSCot);
                                }
                                else
                                {
                                    //Funcion de emision  
                                }
                                break;
                            }
                    }

                    //Aqui va la funcion de validacion Validacion = ValCotizacion(ObjData);
                    return salida;
                } catch (Exception exc)
                {
                    seguro.codigoError = exc.Message;
                    return JsonConvert.SerializeObject(seguro);
                }
            }
            else
            {
                return "Error de autenticacion";
            }
        }
    }
}
