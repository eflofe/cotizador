﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.ServiceModel.Security;
using Cotizador.BanorteCotizacion;
using Cotizador.BANORTE;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.IO;
using System.Text;
using System.Web.Services.Description;
using System.Data;

namespace Cotizador
{
    public class BanorteCot
    {
        private static string usuario = "WS3B502";
        private static string oficina = "3B5";
        private static string pass = "11220";
        private static BanorteCotizacion.WSCotizacionBGClient banorte;
        private static BanorteUbicacion ubicacion;
        private static DatosJson datos;
        private LogCotizacion log = new LogCotizacion();
        public string BanorteCotizacionResponse(Seguro seguro , string log)
        {
           try
            {
                string individual = "";
                string formas = "";
                double sumaprimas;
                string res;

                datos  = new DatosJson();
                banorte = new BanorteCotizacion.WSCotizacionBGClient();
                banorte.ClientCredentials.UserName.UserName = usuario + "|" + oficina;
                banorte.ClientCredentials.UserName.Password = pass;
                banorte.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;
                BanorteCotizacion.Parametro parametro= llenarParametro(seguro);
                BanorteCotizacion.TokenResult tk;
                BanorteCotizacion.CotizarAutoResponse cot = new CotizarAutoResponse();

                //-----------------------
                System.Xml.Serialization.XmlSerializer serializer = new XmlSerializer(parametro.GetType());
                MemoryStream ms = new MemoryStream();
                XmlWriter xmlWriter = XmlWriter.Create(ms);
                serializer.Serialize(xmlWriter, parametro);

                ms.Flush();
                ms.Seek(0, SeekOrigin.Begin);
                StreamReader read1 = new StreamReader(ms);
                res = read1.ReadToEnd();

                this.log.UpdateLogRequest(res, log);
                //---------------------

                tk = banorte.CotizarAuto(parametro, BanorteCotizacion.DatoFlotilla.NO,ref cot.sCotizacionXML);

                if (!tk.Ok)
                    throw new ArgumentException("Error al generar la cotizacion general " + tk.Mensaje);
                else
                {
                    this.log.UpdateLogResponse(cot.sCotizacionXML, log);


                    DatosXML cotizacion = null;

                    XmlSerializer serial = new XmlSerializer(typeof(DatosXML));
                    StringReader reader = new StringReader(cot.sCotizacionXML);
                    cotizacion = (DatosXML)serial.Deserialize(reader);


                    
                    sumaprimas = datos.rellenarJSON(seguro, cotizacion);

                    BanorteCotizacion.DatoExtra interesado = getIntersado();
                    tk = banorte.InsertarCotizacionIndividual(parametro, interesado, getCobertura(seguro, cotizacion), getTipoPago(seguro, cotizacion), Convert.ToDecimal(".0"), DatoFlotilla.NO, ref individual);
                    if (!tk.Ok)
                        throw new ArgumentException("Hubo un error en el objeto banorte " + tk.Mensaje);
                    else
                    {
                        tk = banorte.CalcularFormasPago(parametro, getTipoPago(seguro, cotizacion), sumaprimas.ToString(), ".0", ref formas);
                        if (!tk.Ok)
                            throw new ArgumentException("Hubo un error en el objeto banorte " + tk.Mensaje);
                        else
                        {
                            StringReader read = new StringReader(formas);
                            DatosXML fp = (DatosXML)serial.Deserialize(read);

                            seguro.Cotizacion = getCotizacion(seguro, fp, cotizacion);
                            seguro.Cotizacion.IDCotizacion = individual;
                            seguro.Cotizacion.Resultado = "True";
                        }
                    }
                }
                string response = JsonConvert.SerializeObject(seguro);
                this.log.UpdateLogJsonResponse(response, log);
                return response;

            }
            catch (Exception exc)
            {
                throw new ArgumentException("Ah habido un error en la cotizacion " + exc.Message);
            }
        }

        private string getCobertura(Seguro seguro, DatosXML cotizacion)
        {
            string res = "";
            string salida = "";
            DatosXML datos = cotizacion;

            foreach(C c in datos.C)
            {
                if (c.Cid != "EE" && c.Cid != "ASDM" && c.Cid != "EDDM" && c.Cid != "EDRT" && c.Cid != "AS" && c.Cid != "MC")
                {
                    c.LDYP = null;
                    c.SsAs = null;
                    c.COBDEP = null;
                    var emptyNamespaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
                    XmlWriterSettings settings = new XmlWriterSettings();
                    settings.OmitXmlDeclaration = true;

                    System.Xml.Serialization.XmlSerializer serializer = new XmlSerializer(c.GetType());

                    MemoryStream ms = new MemoryStream();


                    XmlWriter xmlWriter = XmlWriter.Create(ms, settings);

                    serializer.Serialize(xmlWriter,c,emptyNamespaces);

                    ms.Flush();
                    ms.Seek(0,SeekOrigin.Begin);
                    StreamReader reader = new StreamReader(ms);
                    res = reader.ReadToEnd();

                    salida += res;
                    salida.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n", "");

                }

            }

            salida = "<?xml version=\"1.0\" encoding=\"utf-0\"?><DatosXML>" + salida + "</DatosXML>";

            return salida;
        }

        public string getTipoPago(Seguro seguro , DatosXML datos)
        {
            string resultado = "";

            foreach(FP fp in datos.FP)
            {
                if(fp.FPid == selectForm(seguro.PeriodicidadDePago))
                {
                    resultado = "<DatosXML><FP><FPid>" + fp.FPid + "</FPid><FPdsc>"+ fp.FPdsc + "</FPdsc><PP>" + fp.PP +  "</PP><PS>"+ fp.PS + "</PS><Iva>" + fp.Iva + "</Iva><IvaOfi>" +fp.IvaOfi+ "</IvaOfi><R>" + fp.R + "</R><D>"  +fp.D+ "</D><NR>" + fp.NR + "</NR><NM>" + fp.NM + "</NM><DR>" + fp.DR + "</DR></FP></DatosXML>";
                    break;
                }
            }
            return resultado;
        }

        public string selectForm(string pago)
        {
            if (pago == "0")
                return "01";
            else if (pago == "11")
                return "04";
            else if (pago == "3")
                return "03";
            else if (pago == "1")
                return "02";
            else
                throw new ArgumentException("Error al seleccionar la forma de pago");
        }


        public Cotizacion getCotizacion(Seguro seguro, DatosXML fp, DatosXML cotizacion)
        {

            try
            {
                Cotizacion cot = new Cotizacion();

                foreach (FP forma in fp.FP)
                {
                    if (forma.FPid == selectForm(seguro.PeriodicidadDePago))
                    {
                        cot.Derechos = forma.DR;
                        cot.Impuesto = double.Parse(forma.Iva);
                        cot.PrimerPago = double.Parse(forma.PP.Replace("$", ""));
                        cot.PagosSubsecuentes = getPagosSub(forma.PS);

                        if (seguro.PeriodicidadDePago == "0")
                            cot.PrimaTotal = cot.PrimerPago;
                        else
                            cot.PrimaTotal = (int.Parse(cot.PagosSubsecuentes) * int.Parse(seguro.PeriodicidadDePago)) + cot.PrimerPago;

                        cot.PrimaNeta = double.Parse(forma.PNT);
                        cot.Recargos = forma.R;
                    }
                }


                return cot;
            }catch(Exception exc)
            {
                throw new ArgumentException("error al guardar los datos de la cotizacion " + exc.Message);
            }
            
        }

        private string getPagosSub(string pS)
        {
            if (pS.Contains("1 Pago de $"))
                return pS.Replace("1 Pago de $", "");
            else if (pS.Contains("3 Pago de $"))
                return pS.Replace("3 Pago de $", "");
            else if (pS.Contains("11 Pago de $"))
                return pS.Replace("11 Pago de $", "");
            else if (pS.Contains("No Aplica"))
                return pS.Replace("No Aplica", "");
            else
                throw new ArgumentException("Error al generar los pagos subsecuentes");
        }

        private DatoExtra getIntersado()
        {
            BanorteCotizacion.DatoExtra interesado = new DatoExtra();
            interesado.Nombres = "Prueba";
            interesado.ApellidoMaterno = "Emision individual";
            interesado.ApellidoPaterno = "Gonzalez";
            interesado.CodigoAgente = "12674";
            interesado.Correo = "pruebas@gmail.com";
            interesado.Edad = "";
            interesado.EdadMenor = "";
            interesado.Estado = 19;
            interesado.Estatura = "";
            interesado.FechaNacimiento = "01/01/1995";
            interesado.FechaNacimientoMenor = "";
            interesado.NombresMenor = "";
            interesado.Peso = "";
            interesado.Sexo = Genero.Masculino;
            interesado.SexoMenor = Genero.Masculino;
            interesado.Telefono = "5588154845";
            return interesado;
        }

        private Parametro llenarParametro(Seguro seguro)
        {
            try
            {
                BanorteCotizacion.Parametro pam = new Parametro();
                ubicacion = new BanorteUbicacion(seguro.Cliente.Direccion.CodPostal , seguro.Vehiculo.Clave);

                pam.AmparaBote = 0;
                pam.AmparaRemolque = 0;
                pam.ArrastraRemolque = 0;
                pam.CodigoEstado = ubicacion.idEdo;
                pam.CodigoMunicipio = ubicacion.idMunicipio;
                pam.CodigoColonia = int.Parse(seguro.Cliente.Direccion.CodPostal);
                pam.DescripcionAdicional = "";
                pam.DescripcionAuto = "";
                pam.DiasCobertura = 0;
                pam.EdadConductor =(seguro.Cliente.Edad);
                pam.EdadConductorAdicional = -1;
                pam.Frontera = 0;
                pam.Modelo = seguro.Vehiculo.Modelo;
                pam.Paquete = ubicacion.paquetes(ubicacion.tipoVehiculo,seguro.Paquete);
                pam.Pasajeros = 0;
                pam.PeriodoVigencia = "0";
                if (seguro.Descuento != 0)
                    pam.PorcentajeDescuento = seguro.Descuento / 100;
                else
                    pam.PorcentajeDescuento = 0;
                pam.PuertoEntrada = "";
                pam.SBG = seguro.Vehiculo.Clave;
                pam.Subramo = ubicacion.tipoVehiculo;
                pam.Tarifa = 0;
                pam.TipoVehiculo = 0;
                pam.TipoVigencia = 3;
                pam.Uso = "01";
                pam.ValorAmparaBote = 0;
                pam.ValorAmparaRemolque = 0;
                pam.ValorDeclarado = 0;
                return pam;
            }
            catch(Exception exc)
            {
                throw new ArgumentException("Hubo un error al rellenar los parametros" + exc.Message);
            }
        }
    }

}


