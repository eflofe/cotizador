﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Cotizador.BANORTE
{
    public class BanorteUbicacion
    {
        private SqlConnection con = new SqlConnection(@"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=Replica_Ali_R;user id = servici1; password = Master@011");
        internal string direccion;
        internal int idEdo;
        internal string edo;
        internal int idMunicipio;
        internal string municipio;
        internal string tipoVehiculo;

        public BanorteUbicacion(string direccion , string clave)
        {

            if (direccion[0] == '0')
                direccion = direccion.Remove(0,1);
            this.direccion = direccion;
            getDatos();
            tipoV(clave);
        }

        private void getDatos()
        {
            try
            {
                con.Open();
                SqlCommand command = new SqlCommand("select * from CatCpostalBanorte where CP = '"+direccion+"'", con);
                SqlDataAdapter da = new SqlDataAdapter(command);
                DataTable dt = new DataTable();

                da.Fill(dt);
                
                foreach(DataRow data in dt.Rows)
                {
                    this.idEdo = int.Parse(data["IdEdo"].ToString());
                    this.edo = data["Edo"].ToString();
                    this.idMunicipio = int.Parse(data["IdMun"].ToString());
                    this.municipio = data["Municipio"].ToString();
                }

                con.Close();
            }catch(Exception exc)
            {
                throw new ArgumentException("error al extraer los datos " + exc.Message);
            }
        }

        public string paquetes(string sub , string paquete)
        {
            if (paquete.ToUpper() == "AMPLIA" && sub == "01")
                return "1";
            else if (paquete.ToUpper() == "AMPLIA" && sub == "02")
                return "4";
            else if (paquete.ToUpper() == "LIMITADA" && sub == "01")
                return "2";
            else if (paquete.ToUpper() == "LIMITADA" && sub == "02")
                return "5";
            else if (paquete.ToUpper() == "RC" && sub == "01")
                return "3";
            else if (paquete.ToUpper() == "RC" && sub == "02")
                return "6";
            else
                throw new ArgumentException("no se encontro el paquete");
        }

        public void tipoV(string clave)
        {
            try
            {
                con.Open();
                SqlCommand command = new SqlCommand("SELECT * FROM [Catalogos] where IDVehiCia = '" + clave + "'", con);
                SqlDataAdapter da = new SqlDataAdapter(command);
                DataTable dt = new DataTable();

                da.Fill(dt);

                foreach (DataRow data in dt.Rows)
                {
                    if (data["Tipo"].ToString() == "AUT")
                        this.tipoVehiculo = "01";
                    else
                        this.tipoVehiculo = "02";
                }
                con.Close();
            }
            catch(Exception exc)
            {
                throw new ArgumentException("Error en la extraccion del tipo de vehiculo " + exc.Message);
            }
        }

    }
}