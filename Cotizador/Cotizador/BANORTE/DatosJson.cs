﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cotizador.BANORTE
{
    public class DatosJson
    {
       public double rellenarJSON(Seguro seguro , DatosXML datos)
        {
            seguro.Aseguradora = "BANORTE";

            double primas = 0.0;
            seguro.Vehiculo.Uso = "PARTICULAR";
            seguro.Cotizacion.IDCotizacion = "";
            seguro.Coberturas = new Coberturas();

            foreach(C cs in datos.C)
            {
                if (cs.Cid == "DM")
                {
                    seguro.Coberturas.DanosMateriales = "-NDAÑOS MATERIALES-S" + cs.SA + "-D" + cs.Ded;
                }
                else if (cs.Cid == "RT")
                {
                    seguro.Coberturas.RoboTotal = "-NROBO TOTAL-S" + cs.SA + "-D" + cs.Ded;
                }
                else if (cs.Cid == "RC")
                {
                    seguro.Coberturas.RC = "-NRESPONSABILIDAD CIVIL-S" + cs.SA + "-D" + cs.Ded;
                }
                else if (cs.Cid == "RCEX")
                {
                    seguro.Coberturas.RCExtension = "-NEXTENCION RC-S" + cs.SA + "-D" + cs.Ded;
                }
                else if (cs.Cid == "SA")
                {
                    seguro.Coberturas.DefensaJuridica = "-NGASTOS LEGALES-S" + cs.SA + "-D" + cs.Ded;
                    seguro.Coberturas.AsitenciaCompleta = "-NASISTENCIA VIAL-S" + cs.SA + "-D" + cs.Ded;
                }
                else if (cs.Cid == "GM")
                {
                    seguro.Coberturas.GastosMedicosEvento = "-NGASTOS MEDICOS EVENTO" + cs.SA + "-D" + cs.Ded;
                }
                if (cs.Cid != "EE" && cs.Cid != "ASDM" && cs.Cid != "EDDM" && cs.Cid != "EDRT" && cs.Cid != "AS" && cs.Cid != "MC")
                    primas += double.Parse(cs.Pr);
            }
            return primas;
        }

        
    }
}