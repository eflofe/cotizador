﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Cotizador.BANORTE
{
    [Serializable()]
    [System.Xml.Serialization.XmlRoot("DatosXML")]
    public class DatosXML
    {
        [XmlElement("C")]
        public C[] C { set; get; }

        [XmlElement("FPOP")]
        public string FPOP { set; get; }

        [XmlElement("FP")]
        public FP[] FP { set; get; }

    }

    public class FP
    {
        [XmlElement("FPid")]
        public string FPid { set; get; }


        [XmlElement("FPdsc")]
        public string FPdsc { set; get; }



        [XmlElement("PP")]
        public string PP { set; get; }



        [XmlElement("PS")]
        public string PS { set; get; }


        [XmlElement("Iva")]
        public string Iva { set; get; }


        [XmlElement("IvaOfi")]
        public string IvaOfi { set; get; }


        [XmlElement("R")]
        public string R { set; get; }

        [XmlElement("D")]
        public string D { set; get; }


        [XmlElement("NR")]
        public string NR { set; get; }


        [XmlElement("NM")]
        public string NM { set; get; }


        [XmlElement("DR")]
        public string DR { set; get; }


        [XmlElement("PNT")]
        public string PNT { set; get; }

    }


    [Serializable()]
    public class C
    {
        [System.Xml.Serialization.XmlElement("Cid")]
        public string Cid { get; set; }

        [XmlElement("Cdsc")]
        public string Cdsc { get; set; }

        [XmlElement("Ded")]
        public string Ded { get; set; }

        [XmlElement("Ord")]
        public string Ord { get; set; }

        [XmlElement("UDI")]
        public string UDI { get; set; }

        [XmlElement("MA")]
        public string MA { get; set; }

        [XmlElement("GMPP")]
        public string GMPP { get; set; }

        [XmlElement("Pr")]
        public string Pr { get; set; }

        [XmlElement("ManCob")]
        public string ManCob { get; set; }

        [XmlElement("OD")]
        public string OD { get; set; }

        [XmlElement("ODF")]
        public string ODF { get; set; }

        [XmlElement("ODCF")]
        public string ODCF { get; set; }

        [XmlArray("ODS")]
        [XmlArrayItem("DSS", typeof(DSS))]
        public DSS[] DSS { get; set; }


        [XmlElement("OC")]
        public string OC { get; set; }

        [XmlElement("OCUP")]
        public string OCUP { get; set; }

        [XmlElement("DP")]
        public string DP { get; set; }

        [XmlArray("SDED")]
        [XmlArrayItem("LDYP" , typeof(LDYP))]
        public LDYP[] LDYP { get; set; }


        [XmlElement("COBDEP")]
        public COBDEP[] COBDEP { get; set; }

        [XmlElement("SsAs")]
        public SsAs[] SsAs { get; set; }

        [XmlElement("SA")]
        public string SA { get; set; }
    }

    public class DSS
    {
        [XmlElement("DSSC")]
        public string DSSC { get; set; }
        [XmlElement("DSSF")]
        public string DSSF { get; set; }
    }

    [Serializable()]
    public class SsAs
    {
        [XmlElement("LS")]
        public string LS { get; set; }

        [XmlElement("OCUP")]
        public string OCUP { get; set; }

        [XmlElement("LP")]
        public string LP { get; set; }

        [XmlElement("OD")]
        public string OD { get; set; }


    }

    [Serializable()]
    public class COBDEP
    {
        [XmlElement("DPPR")]
        public string DPPR { get; set; }

        [XmlElement("OPER")]
        public string OPER { get; set; }


        [XmlElement("FCTOR")]
        public string FCTOR { get; set; }


    }

    [Serializable()]
    public class LDYP
    {
        [XmlElement("LD")]
        public string LD { get; set; }

        [XmlElement("LDID")]
        public string LDID { get; set; }

        [XmlElement("LP")]
        public string LP { get; set; }

        [XmlElement("OD")]
        public string OD { get; set; }
    }
}