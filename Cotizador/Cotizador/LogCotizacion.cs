﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Cotizador
{
    public class LogCotizacion
    {
        SqlConnection con = new SqlConnection(@"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=ReplicaDeAli;user id = servici1; password = Master@011");

        public string InsertLog(string JSONrequest,string Aseguradora,string Marca,string Modelo, string Descripcion,string clave)
        {
            try
            {
                var fechaInicio = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                string query = "Insert into LogWSCotizacion (JSONRequest,RequestWS,ResponseWS,JSONResponse,Aseguradora,Marca,Modelo,Descripcion,clave,FechaInicio,FechaFin)" +
                        "VALUES ('" + JSONrequest + "',NULL,NULL,NULL,'" + Aseguradora + "','" + Marca + "','" + Modelo + "','" + Descripcion + "','"+clave+"'," +
                        "'" + fechaInicio+ "',NULL)";
                    SqlCommand sqlQuery = new SqlCommand(query);
                
                    sqlQuery.Connection = con;
                    con.Open();
                    sqlQuery.CommandTimeout = 5000;
                    sqlQuery.ExecuteNonQuery();
                    con.Close();
                        var IdReturned = GetLastId();
                    return IdReturned;     
            }
            catch (Exception ex)
            {
                throw new ArgumentException("error al insertar el log " + ex.Message);
            }
        }
        public string GetLastId()
        {
            try
            {
                string selectId = "select MAX(idLogWSCot) from LogWSCotizacion";
                SqlCommand sqlQueryId = new SqlCommand(selectId);
                sqlQueryId.Connection = con;
                con.Open();
                sqlQueryId.CommandTimeout = 0;
                sqlQueryId.ExecuteNonQuery();
                string Id = sqlQueryId.ExecuteScalar().ToString();
                con.Close();
                return Id;
            }
            catch (Exception e)
            {
                return "Error" + e;
            }
        }
        public bool UpdateLogRequest(string XmlRequest,string id)
        {
            try
            {
                string queryUpdate = "Update LogWSCotizacion set RequestWS ='"+XmlRequest+ "' WHERE idLogWSCot ="+id;
                SqlCommand QueryUpdateXML = new SqlCommand(queryUpdate);
                QueryUpdateXML.Connection = con;
                con.Open();
                QueryUpdateXML.CommandTimeout = 0;
                QueryUpdateXML.ExecuteNonQuery();
                con.Close();
                return true;
            }
            catch (Exception e)
            {
                throw new ArgumentException("error al hacer el uptade XML request" + e.Message);
            }
        }
        public bool UpdateLogResponse(string XmlResponse,string id)
        {
            try
            {
                string queryUpdate = "Update LogWSCotizacion set ResponseWS ='" + XmlResponse + "' WHERE idLogWSCot =" + id;
                SqlCommand QueryUpdateXML = new SqlCommand(queryUpdate);
                QueryUpdateXML.Connection = con;
                con.Open();
                QueryUpdateXML.CommandTimeout = 0;
                QueryUpdateXML.ExecuteNonQuery();
                con.Close();
                return true;
            }
            catch (Exception e)
            {
                throw new ArgumentException("error al hacer el uptade XML response" + e.Message);
            }
        }
        public bool UpdateLogJsonResponse(string JsonResponse , string id)
        {
            try
            {
                var fechaFin = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                string queryUpdate = "Update LogWSCotizacion set JSONResponse ='" + JsonResponse + "', FechaFin='"+fechaFin +"' WHERE idLogWSCot =" + id;
                SqlCommand QueryUpdateXML = new SqlCommand(queryUpdate);
                QueryUpdateXML.Connection = con;
                con.Open();
                QueryUpdateXML.CommandTimeout = 0;
                QueryUpdateXML.ExecuteNonQuery();
                con.Close();
                return true;
            }
            catch (Exception e)
            {
                throw new ArgumentException("error al hacer el uptade JSON request" + e.Message);
            }
        }
    }
}