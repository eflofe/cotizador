﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Cotizador
{


    public partial class Seguro
    {
        [JsonProperty("Aseguradora")]
        public string Aseguradora { get; set; } = "";

        [JsonProperty("cliente")]
        public Cliente Cliente { get; set; }

        [JsonProperty("vehiculo")]
        public Vehiculo Vehiculo { get; set; }

        [JsonProperty("Coberturas")]
        public Coberturas Coberturas { get; set; }

        [JsonProperty("paquete")]
        public string Paquete { get; set; } = "";

        [JsonProperty("descuento")]
        public int Descuento { get; set; } = 0;

        [JsonProperty("periodicidadDePago")]
        public string PeriodicidadDePago { get; set; } = "";

        [JsonProperty("cotizacion")]
        public Cotizacion Cotizacion { get; set; }

        [JsonProperty("emision")]
        public Emision Emision { get; set; }

        [JsonProperty("pago")]
        public Pago Pago { get; set; }

        [JsonProperty("CodigoError")]
        public string codigoError { get; set; } = "";

        [JsonProperty("urlRedireccion")]
        public string urlRedireccion { get; set; } = "";

    }

    public class Coberturas
    {
        [JsonProperty("DanosMateriales")]
        public string DanosMateriales { get; set; } = "";

        [JsonProperty("DanosMaterialesPP")]
        public string DanosMaterialesPP { get; set; } = "";

        [JsonProperty("RoboTotal")]
        public string RoboTotal { get; set; } = "";

        [JsonProperty("RCBienes")]
        public string RCBienes { get; set; } = "";

        [JsonProperty("RCPersonas")]
        public string RCPersonas { get; set; } = "";

        [JsonProperty("RC")]
        public string RC { get; set; } = "";

        [JsonProperty("RCFamiliar")]
        public string RCFamiliar { get; set; } = "";

        [JsonProperty("RCExtension")]
        public string RCExtension { get; set; } = "";

        [JsonProperty("RCExtranjero")]
        public string RCExtranjero { get; set; } = "";

        [JsonProperty("RCPExtra")]
        public string RCPExtra { get; set; } = "";

        [JsonProperty("AsitenciaCompleta")]
        public string AsitenciaCompleta { get; set; } = "";

        [JsonProperty("DefensaJuridica")]
        public string DefensaJuridica { get; set; } = "";

        [JsonProperty("GastosMedicosOcupantes")]
        public string GastosMedicosOcupantes { get; set; } = "";

        [JsonProperty("MuerteAccidental")]
        public string MuerteAccidental { get; set; } = "";

        [JsonProperty("GastosMedicosEvento")]
        public string GastosMedicosEvento { get; set; } = "";

        [JsonProperty("Cristales")]
        public string Cristales { get; set; } = "";
    }

    public partial class Cliente
    {
        [JsonProperty("TipoPersona")]
        public string TipoPersona { get; set; } = "";

        [JsonProperty("Nombre")]
        public string Nombre { get; set; } = "";

        [JsonProperty("ApellidoPat")]
        public string ApellidoPat { get; set; } = "";

        [JsonProperty("ApellidoMat")]
        public string ApellidoMat { get; set; } = "";

        [JsonProperty("RFC")]
        public string RFC { get; set; } = "";

        [JsonProperty("fechaNacimiento")]
        public string FechaNacimiento { get; set; } = "";

        [JsonProperty("Ocupacion")]
        public string Ocupacion { get; set; } = "";

        [JsonProperty("CURP")]
        public string CURP { get; set; } = "";

        [JsonProperty("direccion")]
        public Direccion Direccion { get; set; }

        [JsonProperty("edad")]
        public short Edad { get; set; } = 0;

        [JsonProperty("genero")]
        public string Genero { get; set; } = "";

        [JsonProperty("telefono")]
        public string Telefono { get; set; } = "";

        [JsonProperty("Email")]
        public string Email { get; set; } = "";
    }

    public partial class Direccion
    {
        [JsonProperty("Calle")]
        public string Calle { get; set; } = "";


        [JsonProperty("NoExt")]
        public string NoExt { get; set; } = "";


        [JsonProperty("NoInt")]
        public string NoInt { get; set; } = "";


        [JsonProperty("Colonia")]
        public string Colonia { get; set; } = "";


        [JsonProperty("codPostal")]
        public string CodPostal { get; set; } = "";

        [JsonProperty("Poblacion")]
        public string Poblacion { get; set; } = "";

        [JsonProperty("Ciudad")]
        public string Ciudad { get; set; } = "";

        [JsonProperty("Pais")]
        public string Pais { get; set; } = "";
    }

    public partial class Emision
    {
        [JsonProperty("PrimaTotal")]
        public string PrimaTotal { get; set; } = "";

        [JsonProperty("PrimaNeta")]
        public string PrimaNeta { get; set; } = "";

        [JsonProperty("Derechos")]
        public string Derechos { get; set; } = "";

        [JsonProperty("Impuesto")]
        public string Impuesto { get; set; } = "";

        [JsonProperty("Recargos")]
        public string Recargos { get; set; } = "";

        [JsonProperty("PrimerPago")]
        public string PrimerPago { get; set; } = "";

        [JsonProperty("PagosSubsecuentes")]
        public string PagosSubsecuentes { get; set; } = "";

        [JsonProperty("IDCotizacion")]
        public string IDCotizacion { get; set; } = "";

        [JsonProperty("Terminal")]
        public string Terminal { get; set; } = "";

        [JsonProperty("Documento")]
        public string Documento { get; set; } = "";

        [JsonProperty("Poliza")]
        public string Poliza { get; set; } = "";

        [JsonProperty("Resultado")]
        public string Resultado { get; set; } = "";
    }

    public partial class Cotizacion
    {
        [JsonProperty("PrimaTotal")]
        public double PrimaTotal { get; set; } = 0;

        [JsonProperty("PrimaNeta")]
        public double PrimaNeta { get; set; }= 0;

        [JsonProperty("Derechos")]
        public string Derechos { get; set; } = "";

        [JsonProperty("Impuesto")]
        public double Impuesto { get; set; } = 0;

        [JsonProperty("Recargos")]
        public string Recargos { get; set; } = "";

        [JsonProperty("PrimerPago")]
        public double PrimerPago { get; set; } = 0;

        [JsonProperty("PagosSubsecuentes")]
        public string PagosSubsecuentes { get; set; } = "";

        [JsonProperty("IDCotizacion")]
        public string IDCotizacion { get; set; } = "";

        [JsonProperty("CotID")]
        public string CotID { get; set; } = "";

        [JsonProperty("VerID")]
        public string VerID { get; set; } = "";

        [JsonProperty("CotIncID")]
        public string CotIncID { get; set; } = "";

        [JsonProperty("VerIncID")]
        public string VerIncID { get; set; } = "";

        [JsonProperty("Resultado")]
        public string Resultado { get; set; } = "";
    }

    public partial class Pago
    {
        [JsonProperty("MedioPago")]
        public string MedioPago { get; set; } = "";


        [JsonProperty("NombreTarjeta")]
        public string NombreTarjeta { get; set; } = "";


        [JsonProperty("Banco")]
        public string Banco { get; set; } = "";


        [JsonProperty("NoTarjeta")]
        public string NoTarjeta { get; set; } = "";


        [JsonProperty("MesExp")]
        public string MesExp { get; set; } = "";


        [JsonProperty("AnioExp")]
        public string AnioExp { get; set; } = "";


        [JsonProperty("CodigoSeguridad")]
        public string CodigoSeguridad { get; set; } = "";


        [JsonProperty("NoClabe")]
        public string AnioENoClabexp { get; set; } = "";


        [JsonProperty("Carrier")]
        public string Carrier { get; set; } = "";
    }

    public partial class Vehiculo
    {

        [JsonProperty("Uso")]
        public string Uso { get; set; } = "";

        [JsonProperty("marca")]
        public string Marca { get; set; } = "";

        [JsonProperty("modelo")]
        public int Modelo { get; set; } = 0;

        [JsonProperty("NoMotor")]
        public string NoMotor { get; set; } = "";


        [JsonProperty("NoSerie")]
        public string NoSerie { get; set; } = "";


        [JsonProperty("NoPlacas")]
        public string NoPlacas { get; set; } = "";


        [JsonProperty("descripcion")]
        public string Descripcion { get; set; } = "";


        [JsonProperty("CodMarca")]
        public string CodMarca { get; set; } = "";

        [JsonProperty("CodDescripcion")]
        public string CodDescripcion { get; set; } = "";

        [JsonProperty("CodUso")]
        public string CodUso { get; set; } = "";

        [JsonProperty("clave")]
        public string Clave { get; set; } = "";

        [JsonProperty("servicio")]
        public string Servicio { get; set; } = "";


    }
}