﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cotizador.LaLatino
{
    public class LatinoCot
    {
        private long usr = 10041;
        private string pass = "CD56JH34ED23MRVC92";
        private string usrlog = "9257";
        private string usrpss = "L4t1nO9257";

        public string CotizacionLatino(Seguro seguro, string logWSCot)
        {
            try
            {
                LatinoWS.CotizadorLatinoClient cotizador = new LatinoWS.CotizadorLatinoClient();
                LatinoWS.DatosCotizar datos = new LatinoWS.DatosCotizar();

                LatinoWS.Credencial cred = new LatinoWS.Credencial();
                cred.IdApp = this.usr;
                cred.PassApp = this.pass;
                cred.ClaveUsuario = this.usrlog;
                cred.Password = this.usrpss;

                LatinoWS.CaracteristicasCotizacion caracteristicas = new LatinoWS.CaracteristicasCotizacion();
                caracteristicas.ClaveAgente = "137";
                caracteristicas.ClaveDescuento = "0";
                caracteristicas.ClaveEstado = getEstado(seguro.Cliente.Direccion);
                caracteristicas.ClaveServicio = "1";
                caracteristicas.ClaveVigencia = "0";


                //cotizador.Cotizar();

                string response = JsonConvert.SerializeObject(seguro);
                return response;
            }
            catch(Exception exc)
            {
                throw new ArgumentException("Ha habido un error en la cotizacion" + exc.Message);
            }
        }

        private string getEstado(Direccion direccion)
        {
            throw new NotImplementedException();
        }
    }
}