﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cotizador
{
   
    public class CotizadorQualitas
    {
        LogCotizacion lg = new LogCotizacion();
        string respuestaWS = "";
        string Negocio = "2886";
        string TipoMovimiento = "2";
        string FecIniVig = DateTime.Now.ToString("yyyy-MM-dd");
        string FecFinVig = DateTime.Now.AddYears(1).ToString("yyyy-MM-dd");
        string Paquete = "";
        string Agente = "71315";
        string FormaPago = "";
        string Tarifa = "LINEA";
        string DigitoAmis;
        string uso = "";
        string servicio = "";
        
        public string CotizaQualitas(Seguro seguro, string log)
        {
            string CveAmis = seguro.Vehiculo.Clave.ToString();
            
            if (CveAmis.Length == 4)
            {
                CveAmis = "0" + CveAmis;
                DigitoAmis = CveAmis;
            }else if (CveAmis.Length ==3)
            {
                CveAmis = "00" + CveAmis;
                DigitoAmis = CveAmis;
            }
            else
            {
                DigitoAmis = CveAmis;
            }
            if (seguro.Paquete == "AMPLIA")
            {
                Paquete = "01";
            }else if (seguro.Paquete == "LIMITADA")
            {
                Paquete = "03";
            }
            if (seguro.Vehiculo.Uso == "PARTICULAR")
            {
                uso = "01";
            }
            if (seguro.Vehiculo.Servicio == "PARTICULAR")
            {
                servicio = "01";
            }
            string RC = "3000000";
            string GM = "200000";
            string DM = "5";
            string RT = "10";
            string AO = "50000";
            
            return "En construcción, hombres trabajando :v";
        }
       
    }
}